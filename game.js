class Game {

	constructor(game){
		this.game = game;
		this.unavalibleCards = [];
		this.currentCard = null;
		this.currentTurn = 0;
		this.cardsToPull = 0;
		this.direction = true;
		this.oneDroper = null;

		this.getPlayers = this.getPlayers.bind(this);
		this.getCurrentCard = this.getCurrentCard.bind(this);
		this.startGame = this.startGame.bind(this);
		this.dealCards = this.dealCards.bind(this);
		this.getRandomCard = this.getRandomCard.bind(this);
		this.isCardUnavalible = this.isCardUnavalible.bind(this);
		this.dropCard = this.dropCard.bind(this);
		this.requestCard = this.requestCard.bind(this);
		this.skypeTurn = this.skypeTurn.bind(this);
		this.nextTurn = this.nextTurn.bind(this);
		this.challenge = this.challenge.bind(this);

		this.findUserIndex = this.findUserIndex.bind(this);
		this.removeCard = this.removeCard.bind(this);
		this.removeCardFromUnavalible = this.removeCardFromUnavalible.bind(this);
		this.playerAsGame = this.playerAsGame.bind(this);
		
		setTimeout(()=>{
			this.dealCards();
		},1000);
	}

	getPlayers(){
		return this.game.players.map(playerAsGame);
	}
	getCurrentCard(){
		return this.currentCard;
	}

	startGame(){
		this.currentCard = null;
		while(this.currentCard == null){
			let random = this.getRandomCard();
			if(random.number !== 1){
				this.currentCard = random;
			}
		}
		this.unavalibleCards = [...this.unavalibleCards, this.currentCard];
		this.game.players.forEach((player, index)=> {
			player.client.send(JSON.stringify({
				type: "GAME_STARTED",
				result: true,
				data: {
					currentCard: this.currentCard,
					players: this.game.players.map(this.playerAsGame),
					currentTurn: this.currentTurn,
				}
			}));
		});
	}

	dealCards(){
		this.game.players.forEach((player)=>{
			player.cards = [];
			for(let i = 0; i < 7; i++){
				let newCard = this.getRandomCard();
				if(newCard){
					player.cards = [...player.cards, newCard];
					this.unavalibleCards = [...this.unavalibleCards, newCard];
				}
			}
		});

		this.startGame();
	}

	getRandomCard(){
		let result = null;
		let cards = this.unavalibleCards;
		if(this.currentCard){
			cards = [...cards, this.currentCard];
		}

		if(cards.length > 50) return null;

		while(result == null){
			const number = Math.floor(Math.random() * 13) + 1;
			const team = Math.floor(Math.random() * 4) + 1;
			const card = {number, team};

			if(!this.isCardUnavalible(card)){
				result = card;
			}
		}
		return result;
	}

	isCardUnavalible(card){
		let result = false;
		this.unavalibleCards.forEach((value, index) => {
			if(!result){
				if(card.number == value.number && card.team == value.team){
					result = true;
				}
			}
		});

		return result;
	}

	dropCard(payload){
		const {user} = payload;
		let {card} = payload;
		
		card = card || payload.oldCard;
		//PROTOCOL A
		this.oneDroper = card.number === 1 ? user.name : null;

		//PROTOCOL J
		if(card.number === 11){
		}else{
			this.nextTurn();
		}
		
		//PROTOCOL 2
		this.cardsToPull = card.number == 2 ? this.cardsToPull + 2 : 0;

		//PROTOCOL Q

		if(card.number == 12){
			this.direction = !this.direction;
		}

		//PROTOCOL 7
		if(card.number === 7){
			const {oldCard, newCard} = payload;
			this.removeCard(user, oldCard);
			this.removeCardFromUnavalible(oldCard);
			this.currentCard = newCard;
		}else{
			this.removeCard(user, card);
			this.removeCardFromUnavalible(card);
			this.currentCard = card;
		}

		this.game.players.forEach((player, index) => {
			player.client.send(JSON.stringify({
				type: "DROP_CARD",
				result: true,
				data: {
					currentCard: this.currentCard,
					players: this.game.players.map(this.playerAsGame),
					currentTurn: this.currentTurn,
					cardsToPull: this.cardsToPull,
					oneDroper: this.oneDroper,
				}
			}));
		});
		
		if(user.cards.length == 0){
			this.game.players.forEach((player, index) => {
				player.client.send(JSON.stringify({
					type: "GAME_OVER",
					result: true,
					data:{
						winner: user.name,
					}
				}));
			});
		}

	}
	requestCard(payload){
		const {user} = payload;
		const indexWillUpdate = this.findUserIndex(user);
		if(this.unavalibleCards.length > 50){
			this.game.players.forEach((player, index) => {
				player.client.send(JSON.stringify({
					type: "NO_MORE_CARDS",
					result: true,
				}));
			});
		}else{
			const newCard = this.getRandomCard();
			this.game.players[indexWillUpdate].safe = false;
			if(newCard){
				this.game.players[indexWillUpdate].cards = [...this.game.players[indexWillUpdate].cards, newCard];
				this.unavalibleCards = [...this.unavalibleCards, newCard];
			}
			this.game.players.forEach((player, index) => {
				player.client.send(JSON.stringify({
					type: "REQUEST_CARD",
					result: true,
					data: {
						players: this.game.players.map(this.playerAsGame)
					}
				}));
			});
		}

		

	}
	skypeTurn(payload){
		const {user} = payload;
		this.nextTurn();
		//PROTOCOL 2
		const indexWillUpdate = this.findUserIndex(user);
		let cardsWillAdd = [];
		for(let i = 0; i < this.cardsToPull; i++){
			const card = this.getRandomCard();
			if(card){
				cardsWillAdd = [...cardsWillAdd, card];
				this.unavalibleCards = [...this.unavalibleCards, card];
			}
		}
		this.game.players[indexWillUpdate].safe = false;
		this.game.players[indexWillUpdate].cards = [...this.game.players[indexWillUpdate].cards, ...cardsWillAdd];
		this.cardsToPull = 0;

		this.game.players.forEach((player, index) => {
			player.client.send(JSON.stringify({
				type: "SKYPE_TURN",
				result: true,
				data: {
					currentTurn: this.currentTurn,
					players: this.game.players.map(this.playerAsGame),
					cardsToPull: this.cardsToPull,
					oneDroper: this.oneDroper,
				}
			}));
		});
	}
	lastCard(payload){
		const {user} = payload;
		const indexWillSkype = this.findUserIndex(user);
		this.game.players[indexWillSkype].safe = true;

		this.game.players.forEach((player, index) => {
			if(index !== indexWillSkype){
				player.client.send(JSON.stringify({
					type: "LAST_CARD",
					result: true,
					data: {
						player: user.name,
					}
				}));
			}
		});
	}
	challenge(payload){
		const {user} = payload;
		const indexWillSkype = this.findUserIndex(user);
		this.game.players.forEach((player, index) => {
			if(!player.safe && player.cards.length == 1){
				const card1 = this.getRandomCard();
				const card2 = this.getRandomCard();
				if(card1 && card2){
					this.unavalibleCards = [...this.unavalibleCards, card1, card2];
					player.cards =  [...player.cards, card1, card2];
				}
				player.client.send(JSON.stringify({
					type: "CHALLENGE",
					result: true,
					data: {
						player: user.name
					}
				}));
			}
			player.client.send(JSON.stringify({
				type: "UPDATE_PLAYERS",
				result: true,
				data:{
					players: this.game.players.map(this.playerAsGame),
				}
			}));
		});
	}

	nextTurn(){
		if(this.direction){
			this.currentTurn = this.currentTurn == this.game.players.length -1 ? 0 : this.currentTurn + 1;
		}else{
			this.currentTurn = this.currentTurn == 0 ? this.game.players.length -1 : this.currentTurn - 1;
		}
	}
	findUserIndex(user){
		let result = null;
		this.game.players.forEach((value, index) => {
			if(value.email == user.email){
				result = index;
			}
		});
		return result;
	}
	removeCard(user, card){
		const indexWillUpdate = this.findUserIndex(user);
		let indexWillDelete = null;
		this.game.players[indexWillUpdate].cards.forEach((value, index) => {
			if(value.number == card.number && value.team == card.team){
				indexWillDelete = index;
			}
		});
		this.game.players[indexWillUpdate].cards.splice(indexWillDelete, 1);
	}
	removeCardFromUnavalible(card){
		let indexWillDelete = null;
		this.unavalibleCards.forEach((value, index) => {
			if(value.number == card.number && value.team == card.team){
				indexWillDelete = index;
			}
		});
		this.unavalibleCards.splice(indexWillDelete, 1);
	}

	playerAsGame(player, index){
		return {
			email : player.email,
			name: player.name,
			cards: player.cards,
		};
	}
}

module.exports = Game;