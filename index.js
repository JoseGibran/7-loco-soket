const WebSocketserver = require('ws').Server;
const atob = require('atob');
const express = require("express");
const http = require("http");

let users = require('./users');
let createdGames = [];

const app = express();
const port = process.env.PORT || 3001;

const server = http.createServer(app)
server.listen(port);
console.log("http server listening on %d", port);

const wss = new WebSocketserver({server});

console.log("Websocket server created");

let game = require('./game');

wss.on('connection', (ws) => {
	ws.on('message', (actionRaw) => {
		const action = JSON.parse(actionRaw);
		const currentUser = action.payload ? (action.payload.token ? getUserByToken(action.payload.token) : null) : null;
		console.log(action.type);

		switch(action.type){			
			case "LOGIN":
				let logedUser = null;
				users.forEach((user, index) => {
					if(user.email == action.payload.email){
						if(user.password == action.payload.password){
							logedUser = user;
							users[index].client = ws;
						}
					}
				});
				if(logedUser){
					const token = new Buffer( JSON.stringify({email: logedUser.email, name: logedUser.name}) ).toString("base64");
					ws.send(JSON.stringify({type: "LOGIN", result: true, token: token}));
				}else{
					ws.send(JSON.stringify({result: false}));	
				}
			break;

			case "RESET_USER":
				if(currentUser){
					let indexWillUpdate = null;
					users.forEach((user, index) => {
						if(user.email == currentUser.email){
							indexWillUpdate = index;
						}
					});
					users[indexWillUpdate].client = ws;
					ws.send(JSON.stringify({type: "RESET_USER", result: true}));

					//should reset client if user is inside a game
					console.log(createdGames);

				}else{
					ws.send(JSON.stringify({result: false}));
				}
			break;

			case "MESSAGE":
				wss.clients.forEach(client => {
					client.send(JSON.stringify({
						type: "MESSAGE",
						result: true,
						data: action.payload,
					}));
				});
			break;

			case "CREATE_GAME": //NEEDS TOKEN, THINK IS BETTER :')
				if(currentUser != null){
					const newGame = {
						players: [currentUser],
						name: currentUser.name,
						email: currentUser.email,
						started: false,
					};

					createdGames = [...createdGames, newGame];

					const indexDidCreate = createdGames.indexOf(newGame);
					users[users.indexOf(currentUser)].joinedTo = indexDidCreate;


					ws.send(JSON.stringify({type: "CREATE_GAME", result: true}));
					wss.clients.forEach(client => {
						client.send(JSON.stringify({type: "GET_CREATED_GAMES", result: true, data: createdGames.map(getGamesAsDisplay)}));
					});

				}else{
					ws.send(JSON.stringify({result: false}));
				}				
			break;

			case "GET_CREATED_GAMES":
				ws.send(JSON.stringify({type: "GET_CREATED_GAMES",result: true, data: createdGames.map(getGamesAsDisplay)}));
			break;

			case "JOIN_GAME":
				if(currentUser != null){
					if(createdGames[getGameIndexByEmail(action.payload.emailGame)]){
						const players = createdGames[getGameIndexByEmail(action.payload.emailGame)].players;
						createdGames[getGameIndexByEmail(action.payload.emailGame)].players = [...players, currentUser];
						users[users.indexOf(currentUser)].joinedTo = getGameIndexByEmail(action.payload.emailGame);

						//UPDATE THE GAMES CREEN
						ws.send(JSON.stringify({type: "JOIN_GAME", result: true}));
						wss.clients.forEach(client => {
							client.send(JSON.stringify({type: "GET_CREATED_GAMES", result: true, data: createdGames.map(getGamesAsDisplay)}));
						});

						//UPDATE THE LOBBIE
						const currentGame = createdGames[getGameIndexByEmail(action.payload.emailGame)];
						currentGame.players.forEach((player, index) => {
							if(player.client){
								player.client.send(JSON.stringify({
									type: "GET_LOBBIE_INFO",
									result: true,
									data: {
										email: currentGame.email,
										players: currentGame.players.map(getPlayerAsLobbie),
									}
								}));
							}
							
						});
					}else{
						ws.send(JSON.stringify({result: false}));
					}
					

				}else{
					ws.send(JSON.stringify({result: false}));
				}
			break;

			case "GET_LOBBIE_INFO":
				if(currentUser != null){
					const currentGame = createdGames[currentUser.joinedTo];
					if(currentGame){
						ws.send(JSON.stringify({
							type: "GET_LOBBIE_INFO",
							result:true,
							data:{
								email: currentGame.email,
								players: currentGame.players.map(getPlayerAsLobbie),
							}
						}));
					
					}else{
						ws.send(JSON.stringify({result: false}));
					}
					
				}else{
					ws.send(JSON.stringify({result: false}));
				}
			break;

			case "LEAVE_LOBBIE":
				if(currentUser != null){
					const currentGame = createdGames[currentUser.joinedTo];
					let indexWillRemove = null;
					if(currentGame){
						if(currentGame.players){
							currentGame.players.forEach((player, index)=> {
								if(player.email == currentUser.email){
									indexWillRemove = index;
								}
							});
							currentGame.players.splice(indexWillRemove, 1);

							if(currentGame.players.length > 0){
								if(currentUser.email == currentGame.email){
									currentGame.name = currentGame.players[0].name;
									currentGame.email = currentGame.players[0].email;
								}
								//UPDATE LOBBIE
								currentGame.players.forEach(player => {
									player.client.send( 
										JSON.stringify({
										type: "GET_LOBBIE_INFO",
										result: true,
										data: {
											email: currentGame.email,
											players: currentGame.players.map(getPlayerAsLobbie)
										}
									}));
								});
							}else{
								let gameIndexWillRemove = null;
								createdGames.forEach((game, index)=> {
									if(game.email == currentGame.email){
										gameIndexWillRemove = index;
									}
								});
								createdGames.splice(gameIndexWillRemove, 1);
								//UPDATE GAMES
								wss.clients.forEach(client => {
									client.send(JSON.stringify({type: "GET_CREATED_GAMES", result: true, data: createdGames.map(getGamesAsDisplay)}));
								});
							}
						}else{
							let gameIndexWillRemove = null;
							createdGames.forEach((game, index)=> {
								if(game.email == currentGame.email){
									gameIndexWillRemove = index;
								}
							});
							createdGames.splice(gameIndexWillRemove, 1);
							//UPDATE GAMES
							wss.clients.forEach(client => {
								client.send(JSON.stringify({type: "GET_CREATED_GAMES", result: true, data: createdGames.map(getGamesAsDisplay)}));
							});
						}
					}else{
						ws.send(JSON.stringify({result: false}));
					}
				}else{
					ws.send(JSON.stringify({result: false}));
				}
			break;

			case "START_GAME":
				if(currentUser){
					const currentGame = createdGames[currentUser.joinedTo];
					if(currentGame){
						currentGame.players.forEach((player, index) => {
							player.client.send(JSON.stringify({
								type: "START_GAME",
								result: true,
							}));
						});
						createdGames[currentUser.joinedTo].handler = new game(currentGame);
						
						//UPDATE THE GAMES
						const indexWillUpdate = createdGames.indexOf(currentGame);
						createdGames[indexWillUpdate].started = true;

						wss.clients.forEach(client => {
							client.send(JSON.stringify({
								type: "GET_CREATED_GAMES",
								result: true,
								data: createdGames.map(getGamesAsDisplay)
							}));
						});

					}else{
						ws.send(JSON.stringify({result: false}));
					}
				}else{
					ws.send(JSON.stringify({result: false}));
				}
			break;

			case "DROP_CARD":
				if(currentUser){
					const currentGame = createdGames[currentUser.joinedTo];
					action.payload.user = currentUser;
					currentGame.handler.dropCard(action.payload);
				}else{
					ws.send(JSON.stringify({result: false}));
				}
			break;

			case "REQUEST_CARD":
				if(currentUser){
					const currentGame = createdGames[currentUser.joinedTo];
					action.payload.user = currentUser;
					currentGame.handler.requestCard(action.payload);
				}else{
					ws.send(JSON.stringify({result: false}));
				}
			break;

			case "SKYPE_TURN":
				if(currentUser){
					const currentGame = createdGames[currentUser.joinedTo];
					action.payload.user = currentUser;
					if(currentGame.handler){
						currentGame.handler.skypeTurn(action.payload);
					}
				}else{
					ws.send(JSON.stringify({result: false}));
				}
			break;

			case "LAST_CARD":
				if(currentUser){
					const currentGame = createdGames[currentUser.joinedTo];
					action.payload.user = currentUser;
					currentGame.handler.lastCard(action.payload);
				}else{
					ws.send(JSON.stringify({result: false}));
				}
			break;

			case "CHALLENGE":
				if(currentUser){
					const currentGame = createdGames[currentUser.joinedTo];
					action.payload.user = currentUser;
					currentGame.handler.challenge(action.payload);
				}else{
					ws.send(JSON.stringify({result: false}));
				}
			break;


		}
	});

	ws.on('error', (e) => console.log('errored', e));

});


function getClientByEmail(email){
	let client = null;
	users.forEach((user, index)=> {
		if(user.email == email){
			client = user.client;
		}
	});
	return client;
}

//@DEPRECATED
/*function getUserByClient(client){
	let result = null;
	users.forEach((user, index)=> {
		if(user.client == client){
			result = user;
		}
	});
	return result;
}*/

function getUserByToken(token){
	const receivedUser = JSON.parse(atob(token));
	let result = null;
	users.forEach((user, index) => {
		if(receivedUser.email == user.email){
			result = user;
		}
	});
	return result;
}
function getGameIndexByEmail(email){
	let result = null;
	createdGames.forEach((game, index) => {
		if(game.email == email){
			result = index;
		}
	});
	return result;
}

function getGamesAsDisplay(game, index){
	return {
		name: game.name,
		players: game.players.length,
		email: game.email,
		started: game.started || false,
	};
}

function getPlayerAsLobbie(player, index){
	return {
		name: player.name,
		email: player.email,
	}
}