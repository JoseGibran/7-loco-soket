const users = [
	{email: "g@g.com", password: "123456", name: "Gibrán"},
	{email: "d@d.com", password: "123456", name: "David"},
	{email: "n@n.com", password: "123456", name: "Nalumi"},
	{email: "l@l.com", password: "123456", name: "Linsy"},
	{email: "m@m.com", password: "123456", name: "Mama"},
	{email: "p@p.com", password: "123456", name: "Papa"},
	{email: "k@k.com", password: "123456", name: "Kenny"},
	{email: "s@s.com", password: "123456", name: "Steven"},
	{email: "t@t.com", password: "123456", name: "Talo"},
	{email: "r@r.com", password: "123456", name: "Rodrigo"},
	{email: 'joselynecarreraq@gmail.com', password: '123456', name:'Joselyne'},
	{email: 'karen@tipsy.com', password: '123456', name: "Karen"},
	{email: 'iva@tipsy.com', password: '123456', name: 'Ivannoba'},
	{email: 'daniela@tipsy.com', password: '123456', name: 'Daniela'},
	{email: 'ishkur@tipsy.com', password: '123456', name: 'Iskur'},
];

module.exports = users;